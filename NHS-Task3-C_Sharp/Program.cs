﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NHS_Task3_C_Sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = File.ReadAllLines(@"C:\Users\cliya\Desktop\nhs\import_data.csv").Select(a => a.Split(','));
            var csv = from line in lines
                      select (from piece in line
                              select piece);

            var validRecords = from item in csv.Skip(1)
                               where ValidatePostCode(item.ToList()[1])
                               orderby int.Parse(item.ToList()[0])
                               select item;

            var invalidRecords = from item in csv.Skip(1)
                                 where !ValidatePostCode(item.ToList()[1])
                                 orderby int.Parse(item.ToList()[0])
                                 select item;

            using (var file = new StreamWriter(@"C:\Users\cliya\Desktop\nhs\succeeded_validation.csv", true))
            {
                var header = string.Format("{0},{1}", csv.FirstOrDefault().ToList()[0], csv.FirstOrDefault().ToList()[1]);
                file.WriteLine(header);

                foreach (var item in validRecords.Skip(1))
                {
                    if (ValidatePostCode(item.ToList()[1]))
                    {
                        var row = string.Format("{0},{1}", item.ToList()[0], item.ToList()[1]);
                        file.WriteLine(row);
                    }

                }
            }

            using (var file = new StreamWriter(@"C:\Users\cliya\Desktop\nhs\failed_validation.csv", true))
            {
                var header = string.Format("{0},{1}", csv.FirstOrDefault().ToList()[0], csv.FirstOrDefault().ToList()[1]);
                file.WriteLine(header);

                foreach (var item in invalidRecords)
                {
                    var row = string.Format("{0},{1}", item.ToList()[0], item.ToList()[1]);
                    file.WriteLine(row);
                }
            }
        }

        static bool ValidatePostCode(string postCode)
        {
            Regex regex = new Regex(@"^((GIR\s0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9](?<!(BR|FY|HA|HD|HG|HR|HS|HX|JE|LD|SM|SR|WC|WN|ZE)[0-9])[0-9])|([A-PR-UWYZ][A-HK-Y](?<!AB|LL|SO)[0-9])|(WC[0-9][A-Z])|(([A-PR-UWYZ][0-9][A-HJKPSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))))\s[0-9][ABD-HJLNP-UW-Z]{2}))$");

            Match match = regex.Match(postCode);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
